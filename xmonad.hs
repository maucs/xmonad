import XMonad
import XMonad.Operations
import XMonad.Actions.DwmPromote

import XMonad.Layout
import XMonad.Layout.NoBorders
import XMonad.Layout.Tabbed
import XMonad.Layout.ResizableTile
import XMonad.Layout.HintedGrid
import XMonad.Layout.MouseResizableTile

import XMonad.Prompt
import XMonad.Prompt.Shell
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.SetWMName
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import XMonad.Util.Loggers

import qualified Data.Map as M
import Data.Bits ((.|.))
import Data.Ratio
import Graphics.X11
import System.IO

barColor = "#7dc1cf"

main = do
	xmproc <- spawnPipe "xmobar ~/.xmonad/xmobarrc"
	xmonad $ defaultConfig {
		keys = newKeys
                , modMask = mod4Mask
                , borderWidth = 5
                , focusedBorderColor = "darkred"
		, manageHook = manageDocks
                , startupHook = setWMName "LG3D"
                , layoutHook = avoidStruts (mouseResizableTile { masterFrac = 2/3
                                                               , draggerType = FixedDragger 10 10}
                                            ||| ResizableTall 1 (3/100) (1/2) []
                                            ||| Grid False
                                            ||| Mirror tiled
                                            ||| noBorders Full)
		, workspaces = ["one", "two", "three", "four", "five", "six",
                                "seven", "eight", "nine"]
		, terminal = "urxvtc"
		, logHook = myLogHook >> dynamicLogWithPP xmobarPP {
						ppOutput = hPutStrLn xmproc
						, ppExtras = [extraDate]
						, ppCurrent = xmobarColor "black" barColor . pad
						, ppHidden = xmobarColor barColor "" . pad
						, ppLayout = xmobarColor "black" barColor . pad
						, ppWsSep = ""
						, ppTitle = xmobarColor "black" barColor . pad . shorten 100 }
		}
		where tiled = Tall 1 (3%100) (1%2)
                      extraDate = xmobarColorL "black" barColor . padL $ logCmd "date"

-- keybindings
defKeys = keys defaultConfig
delKeys x = foldr M.delete           (defKeys x) (toRemove x)
newKeys x = foldr (uncurry M.insert) (defKeys x) (toAdd    x)
-- remove some default key bindings, clashes with custom keybinding
toRemove XConfig{modMask = modm} = [(modm , xK_r)]
toAdd XConfig{modMask = modm} =
 [ ((modm, xK_r), shellPrompt defaultXPConfig) -- dmenu-ish
 , ((modm, xK_i), sendMessage MirrorShrink) -- lower bottom window
 , ((modm, xK_o), sendMessage MirrorExpand) -- smaller window from bottom
 , ((modm, xK_b), sendMessage ToggleStruts) -- on/off bar
 ]

--fade for inactive window
myLogHook = fadeInactiveLogHook fadeAmount
	where fadeAmount = 0.75
